require 'rails/generators'
require 'rails/generators/rails/app/app_generator'

module Brace
  class AppGenerator < Rails::Generators::AppGenerator
    class_option :database, type: :string, aliases: "-d", default: "postgresql",
      desc: "Configure for selected database (options: #{DATABASES.join("/")})"

    class_option :heroku, type: :boolean, aliases: "-H", default: false,
      desc: "Create staging and production Heroku apps"

    class_option :heroku_flags, type: :string, default: "",
      desc: "Set extra Heroku flags"

    class_option :github, type: :string, aliases: "-G", default: nil,
      desc: "Create Github repository and add remote origin pointed to repo"

    class_option :skip_test_unit, type: :boolean, aliases: "-T", default: true,
      desc: "Skip Test::Unit files"

    class_option :skip_turbolinks, type: :boolean, default: true,
      desc: "Skip turbolinks gem"

    class_option :skip_bundle, type: :boolean, aliases: "-B", default: true,
      desc: "Don't run bundle install"

    def finish_template
      invoke :brace_customization
      super
    end

    def brace_customization
      invoke :customize_gemfile
      invoke :setup_development_environment
      invoke :setup_test_environment
      invoke :setup_production_environment
      invoke :setup_secret_token
      invoke :configure_app
      invoke :remove_routes_comment_lines
      invoke :setup_stylesheets
      invoke :setup_angular
      invoke :setup_api
      invoke :copy_miscellaneous_files
      invoke :customize_error_pages
      invoke :setup_stripe
      invoke :setup_git
      invoke :setup_redis
      invoke :setup_sidekiq
      invoke :setup_database
      invoke :create_heroku_apps
      invoke :create_github_repo
      invoke :setup_segment_io
      invoke :setup_bundler_audit
      invoke :outro
    end

    def setup_angular
      build :install_bower
      build :setup_templates
      build :setup_javascripts
      build :setup_angular_routing
      build :create_application_layout
    end

    def customize_gemfile
      build :replace_gemfile
      build :set_ruby_to_version_being_used

      if options[:heroku]
        build :setup_heroku_specific_gems
      end

      bundle_command 'install'
    end

    def setup_database
      say 'Setting up database'

      if 'postgresql' == options[:database]
        build :use_postgres_config_template
      end

      build :create_database
    end

    def setup_development_environment
      say 'Setting up the development environment'
      build :raise_on_delivery_errors
      build :set_test_delivery_method
      build :raise_on_unpermitted_parameters
      build :provide_setup_script
      build :provide_dev_prime_task
      build :configure_generators
      build :configure_i18n_for_missing_translations
      build :configure_action_mailer_development
    end

    def setup_test_environment
      say 'Setting up the test environment'
      build :set_up_factory_girl_for_rspec
      build :set_up_devise_for_rspec
      build :generate_rspec
      build :configure_rspec
      build :configure_sidekiq_for_rspec
      build :enable_database_cleaner
      build :configure_spec_support_features
      build :configure_i18n_for_test_environment
      build :configure_i18n_tasks
      build :configure_action_mailer_in_specs
    end

    def setup_production_environment
      say 'Setting up the production environment'
      build :configure_newrelic
      build :configure_smtp
      build :enable_rack_deflater
      build :setup_asset_host
    end

    def setup_secret_token
      say 'Moving secret token out of version control'
      build :setup_secret_token
    end

    def configure_app
      say 'Configuring app'
      build :configure_action_mailer
      build :configure_time_formats
      build :configure_rack_timeout
      build :configure_eager_load_paths
      build :configure_action_mailer_previews
      build :disable_xml_params
      build :fix_i18n_deprecation_warning
      build :setup_default_rake_task
      build :setup_foreman
    end

    def setup_stylesheets
      say 'Set up stylesheets'
      build :setup_stylesheets
      build :install_bitters
    end

    def setup_api
      say 'Set up json api'
      build :add_base_controller
      build :install_devise_token_auth
      build :install_responders
    end

    def setup_git
      if !options[:skip_git]
        say 'Initializing git'
        invoke :setup_gitignore
        invoke :init_git
      end
    end

    def setup_redis
      say 'Setting up Redis'
      build :add_redis_initializer
    end

    def setup_sidekiq
      say 'Setting up Sidekiq'
      build :add_sidekiq_initializer
    end

    def setup_stripe
      say 'Setting up Stripe'
      build :add_stripe_initializer
    end

    def create_heroku_apps
      if options[:heroku]
        say "Creating Heroku apps"
        build :create_heroku_apps, options[:heroku_flags]
        build :set_heroku_remotes
        build :set_heroku_rails_secrets
        build :set_memory_management_variable
        build :provide_deploy_script
      end
    end

    def create_github_repo
      if !options[:skip_git] && options[:github]
        say 'Creating Github repo'
        build :create_github_repo, options[:github]
      end
    end

    def setup_segment_io
      say 'Setting up Segment.io'
      build :setup_segment_io
    end

    def setup_gitignore
      build :gitignore_files
    end

    def setup_bundler_audit
      say "Setting up bundler-audit"
      build :setup_bundler_audit
    end

    def init_git
      build :init_git
    end

    def copy_miscellaneous_files
      say 'Copying miscellaneous support files'
      build :copy_miscellaneous_files
    end

    def customize_error_pages
      say 'Customizing the 500/404/422 pages'
      build :customize_error_pages
    end

    def remove_routes_comment_lines
      build :remove_routes_comment_lines
    end

    def outro
      say 'Brace your self! You just generated a Rails app!'
      say "Remember to run 'rails generate rollbar' with your token."
    end

    protected

    def get_builder_class
      Brace::AppBuilder
    end

    def using_active_record?
      !options[:skip_active_record]
    end
  end
end
