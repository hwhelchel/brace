module Brace
  class AppBuilder < Rails::AppBuilder
    include Brace::Actions

    def readme
      template 'README.md.erb', 'README.md'
    end

    def raise_on_delivery_errors
      replace_in_file 'config/environments/development.rb',
        'raise_delivery_errors = false', 'raise_delivery_errors = true'
    end

    def set_test_delivery_method
      inject_into_file(
        "config/environments/development.rb",
        "\n  config.action_mailer.delivery_method = :test",
        after: "config.action_mailer.raise_delivery_errors = true",
      )
    end

    def raise_on_unpermitted_parameters
      config = <<-RUBY
    config.action_controller.action_on_unpermitted_parameters = :raise
      RUBY

      inject_into_class "config/application.rb", "Application", config
    end

    def provide_setup_script
      template "bin_setup.erb", "bin/setup", port_number: port, force: true
      run "chmod a+x bin/setup"
    end

    def provide_dev_prime_task
      copy_file 'development_seeds.rb', 'lib/tasks/development_seeds.rake'
    end

    def configure_generators
      config = <<-RUBY

    config.generators do |generate|
      generate.helper false
      generate.javascript_engine false
      generate.request_specs false
      generate.routing_specs false
      generate.stylesheets false
      generate.javascripts false
      generate.helper false
      generate.template_engine :slim
      generate.test_framework :rspec, fixture: true
      generate.fixture_replacement :factory_girl
      generate.view_specs false
    end

      RUBY

      inject_into_class 'config/application.rb', 'Application', config
    end

    def configure_eager_load_paths
      config = <<-RUBY
  config.eager_load_paths += %W(\#{config.root}/lib)
      RUBY

      inject_into_class 'config/application.rb', 'Application', config
    end

    def configure_action_mailer_previews
      config = <<-RUBY
  config.action_mailer.preview_path = "\#{Rails.root}/spec/mailers/previews"
      RUBY

      inject_into_class 'config/application.rb', 'Application', config
    end

    def configure_action_mailer_development
      config = <<-CONFIG
config.action_mailer.delivery_method = :file
      CONFIG

      environment(config, env: 'development')
    end

    def set_up_factory_girl_for_rspec
      copy_file 'factory_girl_rspec.rb', 'spec/support/factory_girl.rb'
    end

    def set_up_devise_for_rspec
      copy_file 'devise_rspec.rb', 'spec/support/devise.rb'
    end

    def configure_newrelic
      template 'newrelic.yml.erb', 'config/newrelic.yml'
    end

    def configure_smtp
      copy_file 'smtp.rb', 'config/smtp.rb'

      prepend_file 'config/environments/production.rb',
        %{require Rails.root.join("config/smtp")\n}

      config = <<-RUBY

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = SMTP_SETTINGS
      RUBY

      inject_into_file 'config/environments/production.rb', config,
        :after => 'config.action_mailer.raise_delivery_errors = false'
    end

    def enable_rack_deflater
      config = <<-RUBY

  # Enable deflate / gzip compression of controller-generated responses
  config.middleware.use Rack::Deflater
      RUBY

      inject_into_file(
        "config/environments/production.rb",
        config,
        after: serve_static_files_line
      )
    end

    def setup_asset_host
      replace_in_file 'config/environments/production.rb',
        "# config.action_controller.asset_host = 'http://assets.example.com'",
        'config.action_controller.asset_host = ENV.fetch("ASSET_HOST", ENV.fetch("HOST"))'

      replace_in_file 'config/initializers/assets.rb',
        "config.assets.version = '1.0'",
        'config.assets.version = (ENV["ASSETS_VERSION"] || "1.0")'

      inject_into_file(
        "config/environments/production.rb",
        '  config.static_cache_control = "public, max-age=#{1.year.to_i}"',
        after: serve_static_files_line
      )
    end

    def setup_secret_token
      template 'secrets.yml', 'config/secrets.yml', force: true
    end

    def disallow_wrapping_parameters
      remove_file "config/initializers/wrap_parameters.rb"
    end

    def create_application_layout
      template 'brace_layout.html.slim.erb',
        'app/views/layouts/application.html.slim',
        force: true
    end

    def use_postgres_config_template
      template 'postgresql_database.yml.erb', 'config/database.yml',
        force: true
    end

    def create_database
      bundle_command 'exec rake db:create db:migrate'
    end

    def replace_gemfile
      remove_file 'Gemfile'
      template 'Gemfile.erb', 'Gemfile'
    end

    def set_ruby_to_version_being_used
      create_file '.ruby-version', "#{Brace::RUBY_VERSION}\n"
    end

    def setup_heroku_specific_gems
      inject_into_file(
        "Gemfile",
        %{\n\s\sgem "rails_stdout_logging"},
        after: /group :staging, :production do/
      )
    end

    def enable_database_cleaner
      copy_file 'database_cleaner_rspec.rb', 'spec/support/database_cleaner.rb'
    end

    def configure_spec_support_features
      empty_directory_with_keep_file 'spec/features'
      empty_directory_with_keep_file 'spec/support/features'
    end

    def configure_rspec
      remove_file "spec/rails_helper.rb"
      remove_file "spec/spec_helper.rb"
      copy_file "rails_helper.rb", "spec/rails_helper.rb"
      copy_file "spec_helper.rb", "spec/spec_helper.rb"
      copy_file "vcr_helper.rb", "spec/vcr_helper.rb"
    end

    def configure_i18n_for_test_environment
      copy_file "i18n.rb", "spec/support/i18n.rb"
    end

    def configure_i18n_for_missing_translations
      raise_on_missing_translations_in("development")
      raise_on_missing_translations_in("test")
    end

    def configure_i18n_tasks
      run "cp $(i18n-tasks gem-path)/templates/rspec/i18n_spec.rb spec/"
      copy_file "config_i18n_tasks.yml", "config/i18n-tasks.yml"
    end

    def configure_sidekiq_for_rspec
      copy_file 'sidekiq_rspec.rb', 'spec/support/sidekiq.rb'
    end

    def configure_action_mailer_in_specs
      copy_file 'action_mailer.rb', 'spec/support/action_mailer.rb'
    end

    def configure_time_formats
      remove_file "config/locales/en.yml"
      template "config_locales_en.yml.erb", "config/locales/en.yml"
    end

    def configure_rack_timeout
      copy_file 'rack_timeout.rb', 'config/initializers/rack_timeout.rb'
    end

    def configure_action_mailer
      action_mailer_host "development", %{"localhost:#{port}"}
      action_mailer_host "test", %{"www.example.com"}
      action_mailer_host "production", %{ENV.fetch("HOST")}
    end

    def fix_i18n_deprecation_warning
      config = <<-RUBY
    config.i18n.enforce_available_locales = true
      RUBY

      inject_into_class 'config/application.rb', 'Application', config
    end

    def generate_rspec
      generate 'rspec:install'
    end

    def setup_foreman
      copy_file 'sample.env', '.env'
      copy_file 'Procfile', 'Procfile'
    end

    def setup_stylesheets
      remove_file 'app/assets/stylesheets/application.css'
      copy_file 'application.css.scss',
        'app/assets/stylesheets/application.css.scss'
    end

    def setup_templates
      copy_file 'slim.rb', 'config/initializers/slim.rb'
      copy_file 'templates.js.erb', 'app/assets/javascripts/angular/templates.js.erb'
    end

    def setup_javascripts
      remove_file 'assets/javascripts/application.js'
      template 'application.js.erb', 'assets/javascripts/application.js'
    end

    def setup_angular_routing
      routes = <<-ROUTES
  get "*path", to: "application#index"
  root 'application#index'
      ROUTES

      action = <<-ACTION
  def index
    render text: "", layout: "application"
  end
      ACTION

      inject_into_file('config/routes.rb', routes, before: 'end')
      inject_into_file('app/controllers/application_controller.rb', action, before: 'end')
    end

    def install_devise_token_auth
      generate 'devise_token_auth:install Account auth'
    end

    def install_bitters
      run "bitters install --path app/assets/stylesheets"
    end

    def install_responders
      generate 'responders:install'
      application('config.app_generators.scaffold_controller :responders_controller')
    end

    def install_bower
      template 'bower.json.erb', 'bower.json'
      copy_file 'sample.bowerrc', '.bowerrc'
      application("config.assets.paths << Rails.root.join('vendor', 'assets', 'components')
")
      run 'bower install'
    end

    def gitignore_files
      remove_file '.gitignore'
      copy_file 'brace_gitignore', '.gitignore'
      [
        'app/views/pages',
        'spec/lib',
        'spec/controllers',
        'spec/helpers',
        'spec/support/matchers',
        'spec/support/mixins',
        'spec/support/shared_examples'
      ].each do |dir|
        run "mkdir #{dir}"
        run "touch #{dir}/.keep"
      end
    end

    def init_git
      run 'git init'
    end

    def create_heroku_apps(flags)
      rack_env = "RACK_ENV=staging RAILS_ENV=staging"
      rails_serve_static_files = "RAILS_SERVE_STATIC_FILES=true"
      staging_config = "#{rack_env} #{rails_serve_static_files}"
      run_heroku "create #{app_name}-production #{flags}", "production"
      run_heroku "create #{app_name}-staging #{flags}", "staging"
      run_heroku "config:add #{staging_config}", "staging"
      run_heroku "config:add #{rails_serve_static_files}", "production"
    end

    def set_heroku_remotes
      remotes = <<-SHELL

# Set up the staging and production apps.
#{join_heroku_app('staging')}
#{join_heroku_app('production')}
      SHELL

      append_file 'bin/setup', remotes
    end

    def join_heroku_app(environment)
      heroku_app_name = "#{app_name}-#{environment}"
      <<-SHELL
if heroku join --app #{heroku_app_name} &> /dev/null; then
  git remote add #{environment} git@heroku.com:#{heroku_app_name}.git || true
  printf 'You are a collaborator on the "#{heroku_app_name}" Heroku app\n'
else
  printf 'Ask for access to the "#{heroku_app_name}" Heroku app\n'
fi
      SHELL
    end

    def set_heroku_rails_secrets
      %w(staging production).each do |environment|
        run_heroku "config:add SECRET_KEY_BASE=#{generate_secret}", environment
      end
    end

    def set_memory_management_variable
      %w(staging production).each do |environment|
        run_heroku "config:add NEW_RELIC_AGGRESSIVE_KEEPALIVE=1", environment
      end
    end

    def provide_deploy_script
      copy_file "bin_deploy", "bin/deploy"

      instructions = <<-MARKDOWN

## Deploying

If you have previously run the `./bin/setup` script,
you can deploy to staging and production with:

    $ ./bin/deploy staging
    $ ./bin/deploy production
      MARKDOWN

      append_file "README.md", instructions
      run "chmod a+x bin/deploy"
    end

    def create_github_repo(repo_name)
      path_addition = override_path_for_tests
      run "#{path_addition} hub create #{repo_name}"
    end

    def setup_segment_io
          copy_file '_analytics.html.erb',
            'app/views/application/_analytics.html.erb'
    end

    def setup_bundler_audit
      copy_file "bundler_audit.rake", "lib/tasks/bundler_audit.rake"
      append_file "Rakefile", %{\ntask default: "bundler:audit"\n}
    end

    def copy_miscellaneous_files
      copy_file "errors.rb", "config/initializers/errors.rb"
      copy_file "json_encoding.rb", "config/initializers/json_encoding.rb"
    end

    def customize_error_pages
      meta_tags =<<-EOS
  <meta charset="utf-8" />
  <meta name="ROBOTS" content="NOODP" />
  <meta name="viewport" content="initial-scale=1" />
      EOS

      %w(500 404 422).each do |page|
        inject_into_file "public/#{page}.html", meta_tags, :after => "<head>\n"
        replace_in_file "public/#{page}.html", /<!--.+-->\n/, ''
      end
    end

    def add_base_controller
      copy_file 'base_controller.rb', 'app/controllers/api/base_controller.rb'
    end

    def remove_routes_comment_lines
      replace_in_file 'config/routes.rb',
        /Rails\.application\.routes\.draw do.*end/m,
        "Rails.application.routes.draw do\nend"
    end

    def disable_xml_params
      copy_file 'disable_xml_params.rb', 'config/initializers/disable_xml_params.rb'
    end

    def setup_default_rake_task
      append_file 'Rakefile' do
        <<-EOS
task(:default).clear
task default: [:spec]

if defined? RSpec
  task(:spec).clear
  RSpec::Core::RakeTask.new(:spec) do |t|
    t.verbose = false
  end
end
        EOS
      end
    end

    def add_redis_initializer
      copy_file 'redis.rb', 'config/initializers/redis.rb'
    end

    def add_sidekiq_initializer
      copy_file 'sidekiq.rb', 'config/initializers/sidekiq.rb'
    end

    def add_stripe_initializer
      copy_file 'stripe.rb', 'config/initializers/stripe.rb'
    end

    private

    def raise_on_missing_translations_in(environment)
      config = 'config.action_view.raise_on_missing_translations = true'

      uncomment_lines("config/environments/#{environment}.rb", config)
    end

    def override_path_for_tests
      if ENV['TESTING']
        support_bin = File.expand_path(File.join('..', '..', 'spec', 'fakes', 'bin'))
        "PATH=#{support_bin}:$PATH"
      end
    end

    def run_heroku(command, environment)
      path_addition = override_path_for_tests
      run "#{path_addition} heroku #{command} --remote #{environment}"
    end

    def generate_secret
      SecureRandom.hex(64)
    end

    def port
      @port ||= [3000, 4000, 5000, 7000, 8000, 9000].sample
    end

    def serve_static_files_line
      "config.serve_static_files = ENV['RAILS_SERVE_STATIC_FILES'].present?\n"
    end
  end
end
