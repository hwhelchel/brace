if Rails.env.test?
  sidekiq_redis = ConnectionPool.new { MockRedis.new }
else
  sidekiq_redis = { :namespace => '#{app_name}' }
end
Sidekiq.configure_client { |config| config.redis = sidekiq_redis }
Sidekiq.configure_server { |config| config.redis = sidekiq_redis }
