RSpec.configure do |config|
  config.around(:each, type: :feature) do |example|
    Sidekiq::Testing.inline! do
      example.run
    end
  end
end
