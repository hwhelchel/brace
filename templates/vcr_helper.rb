require "vcr"

VCR.configure do |c|
  c.cassette_library_dir      = "spec/cassettes"
  c.hook_into                 :webmock
  c.default_cassette_options  = { :record => :once }
  c.configure_rspec_metadata!

  c.before_playback do |interaction|
    interaction.response.body.force_encoding("utf-8")
  end
end
