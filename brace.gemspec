# -*- encoding: utf-8 -*-
$:.push File.expand_path('../lib', __FILE__)
require 'brace/version'
require 'date'

Gem::Specification.new do |s|
  s.required_ruby_version = ">= #{Brace::RUBY_VERSION}"
  s.authors = ['Harry Whelchel']
  s.date = Date.today.strftime('%Y-%m-%d')

  s.description = <<-HERE
Brace is a base Rails project that you can upgrade. It is used by
me to get a jump start on a working app. Thanks thoughtbot for making Suspenders (https://github.com/thoughtbot/suspenders)!
  HERE

  s.email = 'harrywhelchel@gmail.com'
  s.executables = ['brace']
  s.extra_rdoc_files = %w[README.md LICENSE]
  s.files = `git ls-files`.split("\n")
  s.license = 'MIT'
  s.summary = "Rails template adapted from thoughtbot's Suspenders."
  s.homepage = "https://github.com/hwhelchel/brace"
  s.name = 'brace'
  s.rdoc_options = ['--charset=UTF-8']
  s.require_paths = ['lib']
  s.test_files = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.version = Brace::VERSION

  s.add_dependency 'bitters', '~> 0.10.0'
  s.add_dependency 'bundler', '~> 1.3'
  s.add_dependency 'rails', Brace::RAILS_VERSION

  s.add_development_dependency 'rspec', '~> 2.0'
  s.add_development_dependency 'capybara', '~> 2.2', '>= 2.2.0'
end
