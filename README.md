# Brace

Brace is a base Rails application.

## Installation

First install the brace gem:

    gem install brace

Then run:

    brace projectname

This will create a Rails app in `projectname`.

## Gemfile

To see the latest and greatest gems, look at Brace's
[Gemfile](templates/Gemfile.erb), which will be appended to the default
generated projectname/Gemfile.

It includes application gems like:

* [Rollbar](https://github.com/rollbar/rollbar-gem) for exception notification
* [Bourbon](https://github.com/thoughtbot/bourbon) for Sass mixins
* [Bitters](https://github.com/thoughtbot/bitters) for scaffold application styles
* [Devise](https://github.com/plataformatec/devise) for user authentication
* [Stripe](https://github.com/stripe/stripe-ruby) for payments
* [Sidekiq](https://github.com/mperham/sidekiq) for background
  processing
* [Email Validator](https://github.com/balexand/email_validator) for email
  validation
* [High Voltage](https://github.com/thoughtbot/high_voltage) for static pages
* [jQuery Rails](https://github.com/rails/jquery-rails) for jQuery
* [Neat](https://github.com/thoughtbot/neat) for semantic grids
* [New Relic RPM](https://github.com/newrelic/rpm) for monitoring performance
* [Normalize](https://necolas.github.io/normalize.css/) for resetting browser styles
* [Postgres](https://github.com/ged/ruby-pg) for access to the Postgres database
* [Rack Timeout](https://github.com/kch/rack-timeout) to abort requests that are
  taking too long
* [Refills](https://github.com/thoughtbot/refills) for “copy-paste” components
  and patterns based on Bourbon, Neat and Bitters
* [Title](https://github.com/calebthompson/title) for storing titles in
  translations
* [Puma](https://github.com/puma/puma) to serve HTTP requests

And development gems like:

* [Dotenv](https://github.com/bkeepers/dotenv) for loading environment variables
* [Pry Rails](https://github.com/rweng/pry-rails) for interactively exploring
  objects
* [ByeBug](https://github.com/deivid-rodriguez/byebug) for interactively
  debugging behavior
* [Bundler Audit](https://github.com/rubysec/bundler-audit) for scanning the
  Gemfile for insecure dependencies based on published CVEs
* [Spring](https://github.com/rails/spring) for fast Rails actions via
  pre-loading
* [Better Errors](https://github.com/charliesome/better_errors) for better debugging via
  in-browser IRB consoles.

And testing gems like:

* [Factory Girl](https://github.com/thoughtbot/factory_girl) for test data
* [RSpec](https://github.com/rspec/rspec) for unit testing
* [RSpec Mocks](https://github.com/rspec/rspec-mocks) for stubbing and spying
* [Shoulda Matchers](https://github.com/thoughtbot/shoulda-matchers) for common
  RSpec matchers
* [Timecop](https://github.com/jtrupiano/timecop-console) for testing time

## Other goodies

Bower components:

* [Angular](https://github.com/angular) for client side web applications
* [Ui Router](https://github.com/angular-ui/ui-router) for state transitions
* [Restangular](https://github.com/mgonto/restangular) for RESTful resources
* [Angular Bacon](https://github.com/lauripiispanen/angular-bacon) for FRP yummyness
* [ng-auth-token](https://github.com/lynndylanhurley/ng-token-auth) for api token authentication

Brace also comes with:

* The [`./bin/setup`][setup] convention for new developer setup
* The `./bin/deploy` convention for deploying to Heroku
* Rails' flashes set up and in application layout
* A few nice time formats set up for localization
* `Rack::Deflater` to [compress responses with Gzip][compress]
* A [low database connection pool limit][pool]
* [Safe binstubs][binstub]
* [t() and l() in specs without prefixing with I18n][i18n]
* An automatically-created `SECRET_KEY_BASE` environment variable in all
  environments
* The analytics adapter [Segment][segment] (and therefore config for Google
  Analytics, Intercom, Facebook Ads, Twitter Ads, etc.)

[setup]: http://robots.thoughtbot.com/bin-setup
[compress]: http://robots.thoughtbot.com/content-compression-with-rack-deflater/
[pool]: https://devcenter.heroku.com/articles/concurrency-and-database-connections
[binstub]: https://github.com/thoughtbot/brace/pull/282
[i18n]: https://github.com/thoughtbot/brace/pull/304
[segment]: https://segment.io

## Heroku

You can optionally create Heroku staging and production apps:

    brace app --heroku true

This:

* Creates a staging and production Heroku app
* Sets them as `staging` and `production` Git remotes
* Configures staging with `RACK_ENV` and `RAILS_ENV` environment variables set
  to `staging`
* Adds the [Rails Stdout Logging][logging-gem] gem
  to configure the app to log to standard out,
  which is how [Heroku's logging][heroku-logging] works.

[logging-gem]: https://github.com/heroku/rails_stdout_logging
[heroku-logging]: https://devcenter.heroku.com/articles/logging#writing-to-your-log

You can optionally specify alternate Heroku flags:

    brace app \
      --heroku true \
      --heroku-flags "--region eu --addons newrelic,pgbackups,sendgrid,ssl"

See all possible Heroku flags:

    heroku help create

## Git

This will initialize a new git repository for your Rails app. You can
bypass this with the `--skip-git` option:

    brace app --skip-git true

## GitHub

You can optionally create a GitHub repository for the suspended Rails app. It
requires that you have [Hub](https://github.com/github/hub) on your system:

    curl http://hub.github.com/standalone -sLo ~/bin/hub && chmod +x ~/bin/hub
    brace app --github organization/project

This has the same effect as running:

    hub create organization/project

## Dependencies

Brace requires the latest version of Ruby.

Some gems included in Brace have native extensions. You should have GCC
installed on your machine before generating an app with Brace.

Use [OS X GCC Installer](https://github.com/kennethreitz/osx-gcc-installer/) for
Snow Leopard (OS X 10.6).

Use [Command Line Tools for XCode](https://developer.apple.com/downloads/index.action)
for Lion (OS X 10.7) or Mountain Lion (OS X 10.8).

PostgreSQL needs to be installed and running for the `db:create` rake task.

## Credits

Brace is an extension of [thoughtbot, inc](http://thoughtbot.com/community)'s [suspenders](https://github.com/thoughtbot/suspenders). Thanks thoughtbot for making a great Rails templating gem.

## License

Brace is free software, and may be
redistributed under the terms specified in the LICENSE file.
